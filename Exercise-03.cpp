/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program array dalam manajemen barang;
*tanggal :  4 Maret 2020
*/
#include <iostream>
#include <string.h>
using namespace std;

struct Barang {
   int kbarang;
   char nbarang[30];
   int harga;
   int jumlah;
};

typedef Barang LarikBrg[30];

void banyakData(int& n) {
    cout << "Banyak Barang : "; cin >> n;
}
void inputBarang(LarikBrg& Brg, int n) {
    for (int i = 0; i < n; i++) {
        cout << "Masukkan data barang ke-" << i + 1 << endl;
        cout << "Kode Barang : "; cin >> Brg[i].kbarang;
        cout << "Nama Barang : "; cin >> Brg[i].nbarang;
        cout << "Harga : "; cin >> Brg[i].harga;
        cout << "Jumlah : "; cin >> Brg[i].jumlah;
        cout << endl;
    }
}
void cetakBarang(LarikBrg Brg, int n) {
    cout << "\nPencetakan Data Mahasiswa\n";
	cout << "|Kode|Nama|Harga|Jumlah|" << endl;
    for(int i = 0; i < n; i++){
       cout << "| " << Brg[i].kbarang << " | " <<  Brg[i].nbarang << " | " << Brg[i].harga << " | " << Brg[i].jumlah << " |" << endl; 
    }
    cout << endl;
}

void jumlahsemuabarang(LarikBrg Brg, int n, int jumlah){
	jumlah = 0;
	for (int i = 0; i < n; i++){
		jumlah=jumlah+Brg[i].jumlah;
	} 
	cout << "jumlah semua barang = " << jumlah << endl << endl;
}

void barangTerbanyak(LarikBrg Brg, int n, int max, int lokasi, int i){
 	max = Brg[0].jumlah;
 	i = 1;
  	for(i = 0; i < n; i++){
    	if (Brg[i].jumlah > max){
      		max = Brg[i].jumlah;
    	}
    	lokasi = i + 1 ;
  	}
  	cout << "barang terbanyak adalah barang ke- " << lokasi << " dengan jumlah " << max << endl << endl;
}

void barangTersedikit(LarikBrg Brg, int n, int min, int lokasi, int i){
	min = Brg[0].jumlah;
	i = 1;
	for(i = 0; i < n; i++){
		if (Brg[i].jumlah < min){
			min = Brg[i].jumlah;
		}
		lokasi = i ;
	}
	cout << "barang tersedikit adalah barang ke- " << lokasi << " dengan jumlah " << min << endl << endl;
}

void hargatotalBarang(LarikBrg Brg, int n, int total){
	total = 0;
	for (int i = 0; i < n; i++){
		cout << "Harga total barang- " << i+1 << "= " << (Brg[i].harga)*(Brg[i].jumlah) << endl;
		total = total + ((Brg[i].harga)*(Brg[i].jumlah));
	}
	cout << "harga total barang = " << total << endl;
}

int main() {
   LarikBrg Brg;
   int n,jumlah,max,min,total,i,lokasi;
   banyakData(n);
   inputBarang(Brg, n);
   cetakBarang(Brg, n);
   jumlahsemuabarang(Brg, n, jumlah);
   barangTerbanyak(Brg, n, max, lokasi, i);
   barangTersedikit(Brg, n, max, lokasi, i);
   hargatotalBarang(Brg, n, total);
}

