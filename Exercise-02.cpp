/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program array input + cetak data
*tanggal :  4 Maret 2020
*/
#include <iostream>
#include <string.h>
using namespace std;

struct Mahasiswa {
   int npm;
   char nama[30];
   int umur;
   char kelas[30];
};

typedef Mahasiswa LarikMhs[30];

void banyakData(int& n) {
    cout << "Banyak data : "; cin >> n;
}
void inputMahasiswa(LarikMhs& Mhs, int n) {
    for (int i = 0; i < n; i++) {
        cout << "Masukkan data mahasiswa ke-" << i + 1 << endl;
        cout << "NPM : "; cin >> Mhs[i].npm;
        cout << "Nama : "; cin >> Mhs[i].nama;
        cout << "Umur : "; cin >> Mhs[i].umur;
        cout << "kelas : "; cin >> Mhs[i].kelas;
        cout << endl;
    }
}
void cetakMahasiswa(LarikMhs Mhs, int n) {
    cout << "\nPencetakan Data Mahasiswa\n";

    for(int i = 0; i < n; i++) {
       cout << "Mahasiswa ke- " << i+1 << endl;
       cout << "NPM = " << Mhs[i].npm << endl;
       cout << "Nama = " << Mhs[i].nama << endl;
       cout << "Umur = " << Mhs[i].umur << endl;
       cout << "Kelas = " << Mhs[i].kelas << endl << endl;
    }
}

int main() {
   LarikMhs Mhs;
   int n;
   banyakData(n);
   inputMahasiswa(Mhs, n);
   cetakMahasiswa(Mhs, n);
}






